	
	## 目录
[toc]
### 获取首页地图上显示的站点信息

- URL:object/map2
- 支持的请求类型：POST
		  
### POST 

- 请求格式：
			  
| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |

Example：

```json
		   {
                      "k": "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数据结构   |
| address     | String  |   地址   |
| area       | String  |   区域， 必填，从机构树中复制区域的部分，节点之间用空格隔开   |
| carrier     | String  |   所属运行商， 必填，如电信、移动、联通   |
| code     | String  |   编码   |
| company_name |  int | 代维单位名称|
|exploration     | String  |   探险   |
|fsustatus     | String  |   fsu状态   |
| gprs     | String  |   GPRS   |
| id     | ID  |   装备ID   |
| lat     | double  |   经度   |
| lng     | double  |   纬度   |
|lockset     | Array  |   锁的状态  |
|lockset.btaddr     | String  |   地址   |
| lockset.doorcode     | String  |   门编号   |
| lockset.doortype    | String  |   装备门类型   |
| lockset.id     | String  |   锁ID   |
| lockset.name     | String  |   装备锁具名称   |
|lockset.oid    | String  |   外键，控制器里的属性，站点ID，用于查询相应的控制器  |
|lockset.rfid     | String  |   锁码   |
| lockstatus     | String  |   锁状态  |
 |name     | String  |   名称   |
|overscene     | String  |   覆盖场景   |
|phone     | String  |   电话号码   |
| remark     | String  |  配置   |
| section     | String  |   所属区域  |
 |sid     | String  |  所属区域id   |
| sitecode     | String  |   地理编码   |
|source     | String  |   来源   |
|status     | String  |   状态  |
| type    | String  | 必填，如基站门、集装箱门、拉远站柜门、光交箱柜门、室内设备机柜门等    |

Example：
```json
		{
		s:1
		success:""
		items:
                 address:""
                 area:"新规约测试 事业部测试 测试1 "
                 carrier:"南方电网"
                 code:"C000000217"
				 companyid: 27
                 exploration:[]
                 fsustatus:""
                 gprs:"10"
                 id:225
                 lat:0
                 lng:0
                 lockset:[{				 
                         btaddr:"1234567AB891"
                         doorcode:""
                         doortype:"光交箱门"
                         id:171
                         name:"imei2"
                         oid:231
                         rfid:"3C009F48AC"
                         type:"坚盾Ⅰ"
				 }]
                 name:"测试17"
                 remark:""
                 section:"新规约测试 事业部测试 测试1"
                 sid:105
                 sitecode:"186911"
                 type:"光交箱"
		}
```
### 远程开锁：
 
 - url:  controllerLog/unlock
 - 支持的请求类型：POST 
		  
- 请求格式：
			  
| 参数     | 类型           | 描述                                   | 默认值 | 是否必填 |
| -------- | -------------- | -------------------------------------- | ------ | -------- |
| k        | String           | 用户登录身份验证                       | ""    | YES      |
| oid      | String | 控制器里的属性，站点ID，用于查询相应的控制器 | ""     | YES       |
| doorcode    | String        | 门编号               | ""     | YES       |
| lockName | String         | 锁具名称                               | ""    |     NO     |

Example：

```json 
		   {
                 "k":"2B02553A926396FCDB60818D5132936F",
				 "oid":"211",
				 "doorcode":"A1",
				 "lockName":"测试A1"
           }
```

- 响应格式:JSONObject

| 参数名称 | 参数类型 | 描述                                                 |
| -------- | -------- | ---------------------------------------------------- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

Example：

```json
         {
              s:1
              success:"true"
        }
```

### 获取单位的智能钥匙列表：

- section/{sid}/smartkey
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| k    | String | 经过转换的编码格式，用于获取用户权限 | ""     |  YES        |

Example：

```json
		{
    	 k: "2B02553A926396FCDB60818D5132936F"
		}
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| items       | Array  |   成功返回的数据结构   |
| btaddr    | String  |  蓝牙地址 |
| id | id | 自增量，主键 |
|lose_by| String | 可为空|
| lose_by_name| String|可为空|
| name| String  |  名称 |
| section |String |区域 |
| sid | int |所属区域id |
| status |String | 状态 |
| type | String | 钥匙类型 |
| updatetime |String |更新时间|
| user |String| 用户 |
| verify_by| String | 可为空 |
| verify_by_name| String | 可为空 |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
|page  | int  |   当前的页码|
|pagesize| int |一页里的数量|
|pages| int | 总的页数 |
| total | int |返回的items的长度|

Example：
```json
       {
	     items:Array
	   {
	      btaddr:"D43639594AF0"
	      id:35
	      lose_by:""
	      lose_by_name:""
	      name:"小波的小钥匙f0"
	      section:"TBS区域测试 TBS二级区域测试 "
	      sid:92
	      status:"启用"
	      type:"E402"
	      updatetime:"20180820143811"
	      user :""
	      verify_by:""
	      verify_by_name:""
	  }
	   page: 0
       pages:1
	   pagesize:20
	   s:1
	   success:""
	   total:3
		}
```

### 修改智能钥匙 ：

- smartkey/{kid}/update
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| name    | String |名称| ""     |  YES        |
| section |  String  | 所属单位|""|  YES  |
| sid | String | 所属区域id |""|YES|
|type| String |类型，取常量中的sm_type|  "" |NO|
|user|String|钥匙拥有者，取权限范围内的用户|""|NO|
|btaddr|String|蓝牙地址|""|NO|

Example：

```json
		{
    	 "name": "名称"
         "section":  "所属区域"
         "sid"："所属区域id"
         "type": "类型"
         "user": "钥匙拥有者"
         "btaddr": "蓝牙地址"
		}
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| id | int |修改成功的钥匙的id|
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

Example：
```json
       {
	     "k":"3FDD423BB9F5264FDEE21EE42C6B3C7A",
	     "name":"小波的小钥匙f0",
	     "btaddr":"D43639594AF0",
	     "type":"E401",
	     "user":"",
	     "section":"TBS区域测试 TBS二级区域测试 ",
	     "sid":92
		}
```

### 新建智能钥匙：

- section/{sid}/smartkey/new
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| k | String | 经过转换的编码格式，用于获取用户权限 | ""     |  YES        |
| name    | String |名称| ""     |  YES        |
| section |  String  | 所属单位|""|  YES  |
|type| String |类型，取常量中的sm_type|  "" |NO|
|user|String|钥匙拥有者，取权限范围内用户|""|NO|
|btaddr|String|蓝牙地址|""|NO|

Example：

```json
		{
    	 "name": "名称"
         "section":  "所属区域"
         "type": "类型"
         "user": "钥匙拥有者"
         "btaddr": "蓝牙地址"
		}
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| id | int | 修改成功的钥匙的id |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

Example：
```json
       {
	   "k":"3FDD423BB9F5264FDEE21EE42C6B3C7A",
	   "name":"测试用的钥匙",
	   "btaddr":"F45EABB2697A",
	   "type":"E402",
	   "user":"22222",
	   "section":"TBS区域测试 TBS二级区域测试 TBS三级区域测试 "
		}
```

### 搜索智能钥匙：

- section/{sid}/smartkey
- 支持的请求类型：POST

### post  
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| search    | String |搜索内容| ""     |  YES       |
| page |  int  | 页码|""|  YES  |
|pagesize| int |数量|  "" |YES|


Example：

```json
		{
          "search" : "搜索内容"
          "page": "页码"
          "pagesize" : "数量" 
        }
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| items       | Array  |   成功返回的数据结构   |
| btaddr    | String  |  蓝牙地址 |
| id | id | 自增量，主键 |
|lose_by| String | 可为空|
| lose_by_name| String|可为空|
| name| String  |  名称 |
| section |String |区域 |
| sid | int |所属区域id |
| status |String | 状态 |
| type | String | 钥匙类型 |
| updatetime |String |更新时间|
| user |String| 钥匙拥有者 |
| verify_by| String | 可为空 |
| verify_by_name| String | 可为空 |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
|page  | int  |   当前的页码|
|pagesize| int |一页里的数量|
|pages| int | 总的页数 |
| total | int |返回的items的长度|
Example：
```json
       {
	   items:[
	   [{
	      btaddr:"D43639594AF0"
	      id:35
	      lose_by:""
	      lose_by_name:""
	      name:"小波的小钥匙f0"
	      section:"TBS区域测试 TBS二级区域测试 "
	      sid:92
	      status:"启用"
	      type:"E402"
	      updatetime:"20180820143811"
	      user :""
	      verify_by:""
	      verify_by_name:""
	  }]
	   page: 0
       pages:1
	   pagesize:20
	   s:1
	   success:""
	   total:3
		}
```

###  新增站点：

- section/{sid}/object/new/lockset/zone
- 支持的请求类型：POST

### post
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| name    | String |站点名称| ""     |  YES       |
| area |  String  | 区域|""|  YES  |
|type| String |站点类别，取常量中的obj_type，如室外柜、光交箱、基站机房|  "" |YES|
| lng    | double |经度,可选| ""     |  YES       |
| lat    | double |维度,可选| ""     |  YES       |
| address |  String  | 地址|""|  NO  |
|companyid| String  |代维单位|  "" |NO|
| sitecode    | String |数组值是范围ID| ""     |  NO       |
| remark |  String  | 备注|""|  NO  |
|zid| int 数组 |数量|  "" |YES|
| lockset    | Lockset |锁具| ""     |  YES       |
| lockset.name    | String |锁名称| ""     |  YES       |
| lockset.doortype    | String |门类型，取常量中的door_type，如光交箱门、机柜门、防盗门| ""     |  NO       |
| lockset.doorcode    | String |门编码，固定选项：A1、A2、B1、B2| ""     |  NO       |
| lockset.type   | String |锁类型，取常量中的lock_type，如屏柜门锁、灵性锁| ""     |  YES       |
| lockset.rfid    | String |锁码| ""     |  NO       |
| lockset.btaddr    | String |蓝牙地址| ""     |  NO       |
| controller |  ControllerTool  | 控制器|""|  YES  |
| controller.code |  String  | 控制器ID|""|  NO  |
| controller.type |  String  | 控制器类型，取常量中的controller_type，如：	T50-SC-CDMA-1|""|  NO  |
| controller.work_mode |  String  |控制器工作模式，取常量中的work_mode，如休眠唤醒模式|""|  NO  |
| controller.remark |  String  | 备注|""|  NO  |

Example：

```json
		{
          "name": "站点名称"
          "area": "区域"
          "type": "站点类别，如基站门、集装箱门"
          "lng": "经度"
           "lat": "维度" // 可選
           "address": "地址",
           "companyid": "代维单位" // 可選
            "sitecode": 设备ID
            "remark": 备注
           "zid": [1,2,3,1] // 数组值是范围ID
           "lockset":[{
            "name": "锁名称"
           "doortype": "门类型"
           "doorcode": "门编码"
           "type": "锁类型"
            "rfid": "锁码"
           btaddr": "蓝牙地址"
  }]
          "controller":[{
           "code": "控制器ID"
           "type": "控制器类型"
           "work_mode": "控制器工作模式"
           "remark": "备注"
  }]  // 控制器目前最多只有一个
        }
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| oid    | int | 所属设备,外键 |

Example：
```json
       {
	  "success":"",
	  "s":1,
	  "oid":231
		}
```

### 修改站点：

- object/{oid}/update2
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| name    | String |站点名称| ""     |  YES       |
| area |  String  | 区域|""|  YES  |
|type| String |站点类别，取常量中的obj_type，如室外柜、光交箱、基站机房|  "" |YES|
| lng    | double |经度,可选| ""     |  YES       |
| lat    | double |维度,可选| ""     |  YES       |
| address |  String  | 地址|""|  NO  |
|companyid| String  |代维单位|  "" |NO|
| sitecode    | String |数组值是范围ID| ""     |  NO       |
| remark |  String  | 备注|""|  NO  |
|zid| int 数组 |数量|  "" |YES|
| lockset    | Lockset |锁具| ""     |  YES       |
| lockset.id    | int |锁id(修改锁存在原本ID，新增锁不需要传ID)|   |  NO       |
| lockset.name    | String |锁名称| ""     |  YES       |
| lockset.doortype    | String |门类型，取常量中的door_type，如光交箱门| ""     |  NO       |
| lockset.doorcode    | String |门编码| ""     |  NO       |
| lockset.type   | String |锁类型，取常量中的lock_type，如屏柜门锁	| ""     |  YES       |
| lockset.rfid    | String |锁码| ""     |  NO       |
| lockset.btaddr    | String |蓝牙地址| ""     |  NO       |
| controller |  ControllerTool  | 控制器|""|  YES  |
| controller.code |  String  | 控制器ID|""|  NO  |
| controller.type |  String  | 控制器类型，取常量中的controller_type	，如T50-SC-CDMA-1	|""|  NO  |
| controller.work_mode |  String  |控制器工作模式，取常量中的work_mode，如休眠唤醒模式	|""|  NO  |
| controller.remark |  String  | 备注|""|  NO  |

Example：

```json
		{
          "name": "站点名称"
          "area": "区域"
          "type": "站点类别，如基站门、集装箱门"
          "lng": "经度"
           "lat": "维度" // 可選
           "address": "地址",
           "companyid": "代维单位" // 可選
            "sitecode": 设备ID
            "remark": 备注
           "zid": [1,2,3,1] // 数组值是范围ID
           "lockset":[{
           	"id": "锁ID" // 修改锁存在原本ID，新增锁不需要传ID
            "name": "锁名称"
           "doortype": "门类型"
           "doorcode": "门编码"
           "type": "锁类型"
            "rfid": "锁码"
           btaddr": "蓝牙地址"
  }]
          "controller":[{
           "code": "控制器ID"
           "type": "控制器类型"
           "work_mode": "控制器工作模式"
           "remark": "备注"
  }]  // 控制器目前最多只有一个
        }
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

Example：
```json
       {
	   "success":"",
	   "s":1
		}
```

### 删除站点：

- object/{oid}/del
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型   | 描述                                 | 默认值 | 是否必填 |
| ---- | ------ | ------------------------------------ | ------ | -------- |
| name    | String |站点名称| ""     |  YES       |
|  k     |  String  |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |

Example：

```json
		{
          "k": "657F27A20859B5A1DC5D69EC83DC8D6F"
        }
``` 

- 响应格式:JSONObject

| 参数名称    | 参数类型 | 描述                       |
| ----------- | -------- | -------------------------- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

Example：
```json
       {
	   "success":"",
	   "s":1
		}
```

### 获取主页地图设备信息

- object/map3
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |

Example：

```json
		   {
                      "k": "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数据结构   |
| id     | ID  |   装备ID   |
| lat     | double  |   经度   |
| lng     | double  |   纬度   |
|name     | String  |   名称   |
 

Example：
```json
       {
	   		success: "", 
			s: 1,
			items: [{
							id:  282, 
							name:  "智盾1.3演示",
							lat:  0,
							lng:  0
					}]
		}
```

### 获取权限范围内的用户

- user/getUser
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |

Example：

```json
		   {
                      "k": "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数据结构   |
| username  | String  |   账号   |
| name    | String  |   用户名   |
 
Example：
```json
       {
	   		success: "", 
			s: 1,
			items:[ {
							"username":  "15328041011", 
							"name":  "罗小兵"
					   }]
		}
```



### 获取本级及上级区域的代维单位

- section/companys
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      NO    |
| id     |   int    |    区域id  |    “”      |      YES    |
Example：

```json
		   {
                      k: "657F27A20859B5A1DC5D69EC83DC8D6F",
					  id: "11"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数据结构   |
| items.id  | int  |   代维单位id |
| items.name    | String  |   代维单位用户名   |
| items.sid  |  int  | 区域id|
 
Example：
```json
       {
			items:[ {
							"id":  27, 
							"name":  "代维1"，
							"sid": 91，
					   }]
		}
```
### 用户注册

- user/reg
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| username     |   String   | 手机号码  |   “”      |      YES    |
| password     |   String    |    密码  |   ""      |      YES    |
| name   |   String   | 用户名   |     ""  |  YES  |
|  memo  | String | 描述  | ""  |   NO  |
|   section | int   | 所在区域 | ""|  YES |
|  cid   | int | 代维单位id  |  ""  |  YES |
Example：

```json
		   {                        			           
		   "username":"14444444444",
		   "password":"1",
		   "name":"2222",
		   "memo":"",
		   "section": 92
		   "cid": 27
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| username       | String  |   手机号码   |
 
Example：
```json
       {
			"success":"",
			"s":1,
			"username":"11111111115"
		}
```

### 列出某类型的常量

- dict/{type}/       //例如，dict/controller_type/
- 支持的请求类型：POST

### key - value对应的选择

| key | value | 描述 |
| ----   | ----    |  ----    | 
|  batt  |   0.8    |  电量 |
|  carrier  |   南方电网   |    运营商|
|    |   南方电网    |     |
|  controller_time  |   48    |    控制器更新时间 |
|  controller_type  |   T50-SC-CDMA-1    |    控制器类型 |
|    |   T50-SC-GSM-1    |    |
|    |   T50-SC-NB-1    |     |
|  door_type  |   	光交箱门    |    门类型 |
|    |   机柜门    |     电量 |    |
|    |   防盗门    |   电量 |    |
|  km_key_rowcount  |   4    |     |
|    |   8    |     |
|    |   4    |     |
|  km_keycount  |   8    |      |
|    |   48    |    |
|    |   8    |  |
|  lock_type  |   屏柜门锁    |   锁类型 |
|   |   灵性锁    |    |
|    |   	电控机柜门锁    |   |
|    |   防盗门锁    |   |
|  obj_type  |   光交箱    |  设备类型  |
|    |   	基站机房    |   |
|    |   室外柜	    |   |
|  patrol_item  |   巡检项目1    |  巡检项目 |
|    |   巡检项目2    |   |
|    |   巡检项目3    |   |
|    |   巡检项目4    |   |
|  period  |   周度    |   巡检 周期 | 
|    |   季度    |   |
|    |   年度    |   |
|    |   月度    |   |
|    |   每日    |   |
|  sm_type  |   E401    |   钥匙类型 |
|  sm_type  |   E402    |     |
|  unlock_order  |   FSU    |  解锁优先顺序  |
|    |   智能锁    |    |
|    |   蓝牙    |     |
| unlock_protocol   |   1    |  解锁协议    |
|work_mode|休眠唤醒模式|  工作模式  |
|   |    长供电模式  |      |
|  workdesc  |   第一个上站理由   |   上站理由  |
|    |    第二个上站理由    |    |
|    |   第三个上站理由    |     |
|  doorcode  |   A1   |   门编号  |
|    |   A2    |    |
|    |   B1    |     |
|    |   B2    |     |

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |
Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F",
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数组 |
| 0 |  String|   第一个数据 |
| 1 | String | 第二个数据 |
| 2 | String | 第三个数据 |
 
Example：
```json
       {
			"success":"",
			"s":1,
			items:
			0:"T50-SC-CDMA-1"
			1:"T50-SC-GSM-1"
			2:"T50-SC-NB-1"
		}
```
### 获取某用户的某个sid及其下属section

- section/{username}/getsec  //其中username为账号,当没有账号时请求为0
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      NO    |
Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F",
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| sid       | int  |  用户的区域id   |
| items       | Array  |   成功返回的数组 |
|items. id |  int |   区域id |
|items. intro | String | 描述 |
|items. name | String |名称 |
|items.parents|String| 上一级区域的名称|
 |items. type | String |类型 |
 |items. pid | int |外键 |
 |items.company_names:| 代维单位名称|
 |companys|company|代维单位|
 | companys.id  | int  |   代维单位id |
| companys.name    | String  |   代维单位用户名   |
| companys.sid  |  int  | 区域id|
Example：
```json
       {
			items:
			[{
			id: 93,
			intro: "", 
			name: "TBS三级区域测试", 
			parent: "root"
			type: "区域", 
			pid: 92，
			company_names: "代维1;"
			companys:[{
			id: 27
			name: "代维1"
			sid: 91
			}]
			}]
            s:1
            sid:92
			success:""
		}
```
### 获取某单位及下属单位设备

- section/{sid}/object/downward
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |

Example：

```json
		   {
                      "k": "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| total       | int  |   items的长度   |
| page  | int  |   当前页   |
| pages   | int  |   总页数   |
| pagesize| int | 每页的数量 |
| items       | Array  |   成功返回的数据结构   |
| adress       | String | 设备地址   |
| area      | String  |   设备区域 |
| code   | String  |   设备编号   |
| carrier     | String  |   所属运行商， 必填，如电信、移动、联通   |
| code     | String  |   编码   |
| company_name |  String |  代维单位名称 |
| control_tool_ID     | String  |  控制器id   |
| control_tool_type     | String  |   控制器类型   |
|control_work_mode    | String  |  控制器工作模式   |
| controller | controller| 控制器对象|
| controller.code | String| 控制器id |
| controller.id | int | id,自增量 |
| controller.oid | controller| 控制器|
| controller.remark | String| 控制器备注|
| controller.type | String| 控制器类型|
| controller.work_mode| String| 控制器工作模式|
| gprs     | String  |   GPRS   |
| id     | ID  |   装备ID   |
| lat     | double  |   经度   |
| lng     | double  |   纬度   |
| lockCount| int | 锁的数量|
|lockset     | Array  |   锁的状态  |
|lockset.btaddr     | String  |   地址   |
| lockset.doorcode     | String  |   门编号   |
| lockset.gate_status    | String  |   装备门类型   |
| lockset.id     | String  |   锁ID   |
| lockset.lock_status| String |锁状态|
| lockset.name     | String  |   装备锁具名称   |
|lockset.oid    | String  |   外键，控制器里的属性，站点ID，用于查询相应的控制器  |
|lockset.rfid     | String  |   锁码   |
 |name     | String  |   名称   |
|phone     | String  |   电话号码   |
| remark     | String  |  配置   |
| resourceid     | String  |   系统资源编码   |
| section     | String  |   区域名称字段，app专用  |
 |sid     | String  |  所属区域id   |
| sitecode     | String  |   地理编码   |
| type    | String  | 必填，如基站门、集装箱门、拉远站柜门、光交箱柜门、室内设备机柜门等    |
|zids| Array| 开锁范围内的设备|
|zids.id| int |设备id|
|zids.status| String | 状态|
Example：
```json
       {
	   		success: "", 
			s: 1,
			items: [{
						address:"cyg"
						area:"新规约测试 事业部测试 测试1 "
						carrier:"南方电网"
						code:"C000000217"
						companyid: 27
						control_device_id:"4bfd35a9-75b6-4208-9ed0-8f43c2544ef6"
						control_tool_ID:"18691"
						control_tool_type:"T50-SC-GSM-1"
						control_work_mode:"长供电模式"
						controller:[{
						                 code: "6667895"
						                 id:36
						                 oid:231
						                 remark:""
						                 type:"T50-SC-NB-1"
										 work_mode:"休眠唤醒模式"
						}]
						gprs:"3"
						id:231
						lat:0
						lng:0
						lockCount:3
						lockset:[{
							btaddr:"1234567AB891"
							doorcode:"A1"
							doortype:"光交箱门"
							id:171
							name:"imei2"
							oid:231
							rfid:"3C009F48AC"
							type:"坚盾Ⅰ",
							gate_status:false ,
							lock_status:false
						},
						{
							btaddr:"1234567AB891"
							doorcode:"A2"
							doortype:"光交箱门"
							id:172
							name:"imei2"
							oid:231
							rfid:"3C009F48AC"
							type:"坚盾Ⅰ",
							gate_status:false ,
							lock_status:false
							},
						{
							btaddr:"1234567AB891"
							doorcode:"B1"
							doortype:"光交箱门"
							id:173
							name:"imei2"
							oid:231
							rfid:"3C009F48AC"
							type:"坚盾Ⅰ",
							gate_status:false ,
							lock_status:false
						}]
						name:"站点5"
						remark:""
						section:"TBS区域测试 TBS二级区域测试 TBS三级区域测试 4级"
						sid:94
						sitecode:"11111111111111111"
						type:"建筑门"
						zids:[{
							id:59
							status:"启用"
						}]
					page:0
					pages:1
					pagesize:20
					s:1
					success:""
					total:3
		}
```
### 查询锁的状态

- section/{sid}/object/lockstate   
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |
Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F",
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| items       | Array  |   成功返回的数组 |
|controllerlog| ControllerLog |  日志 |
|controllerlog.gate_status1| boolean | 门1状态，其中TRUE代表开，FALSE为关 |
|controllerlog.gate_status2| boolean | 门2状态 |
|controllerlog.gate_status3| boolean | 门3状态 |
|controllerlog.gate_status4| boolean | 门4状态 |
|controllerlog.gate_status5| boolean | 门5状态 |
|controllerlog.gate_status6| boolean | 门6状态 |
|controllerlog.gate_status7| boolean | 门7状态 |
|controllerlog.gate_status8| boolean | 门8状态 |
|controllerlog.lock_status1| boolean | 锁1状态 |
|controllerlog.lock_status2| boolean | 锁2状态 |
|controllerlog.lock_status3| boolean | 锁3状态 |
|controllerlog.lock_status4| boolean | 锁4状态 |
|controllerlog.lock_status5| boolean | 锁5状态 |
|controllerlog.lock_status6| boolean | 锁6状态 |
|controllerlog.lock_status7| boolean | 锁7状态 |
|controllerlog.lock_status8| boolean | 锁8状态 |
 
Example：
```json
       {
			"success":"",
			"s":1,
			items:[
			controllerlog:{
						batt:12.3
						gate_status1:false      //门状态，TRUE为开，FALSE为关
						gate_status2:true
						gate_status3:true
						gate_status4:true
						gate_status5:false
						gate_status6:false
						gate_status7:false
						gate_status8:false
						id:1341
						lock_status1:false    ////锁状态，TRUE为开，FALSE为关
						lock_status2:true
						lock_status3:true
						lock_status4:true
						lock_status5:false
						lock_status6:false
						lock_status7:false
						lock_status8:false
						lock_type:"1"
						oid:63
							}]
		}
```

### 添加锁具

- object/{oid}/lockset/new                       //oid为设备id   
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |
| name      |   String   | 锁名称 |   “”      |      YES    |
| type      |   String   |锁类型  |   “”      |      YES    |
| rfid      |   String   |  |   “”      |     NO     |
| btname      |   String   |蓝牙名称  |   “”      |   NO       |
| btaddr      |   String   | 蓝牙地址   |   “”      |      NO    |
| doortype      |   String   | 门类型 |   “”      |      NO    |
| doorcode      |   String   | 门编号 |   “”      |     NO     |


Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F",
		   name: "郭的码片",
		   type: "坚盾Ⅰ",
		   rfid: "490093C08F",
		   btname: " ",
		   btaddr: "8030DC0412D1",
		   doortype: "光交箱门",
		   doorcode: "A1"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| id       | int  |  锁具添加成功时返回的锁具id |

 
Example：
```json
       {
			"success":"",
			"s":1,
			“id”: 22
		}
```

### 修改(更新）锁具

- lockset/{lid}/update                       //lid为锁具id   
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |
| name      |   String   | 锁名称 |   “”      |      YES    |
| type      |   String   |锁类型  |   “”      |      YES    |
| rfid      |   String   | 锁码 |   “”      |     NO     |
| btname      |   String   |蓝牙名称  |   “”      |   NO       |
| btaddr      |   String   | 蓝牙地址   |   “”      |      NO    |
| doortype      |   String   | 门类型 |   “”      |      NO    |
| doorcode      |   String   | 门编号 |   “”      |     NO     |


Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F",
		   name: "郭的码片",
		   type: "坚盾Ⅰ",
		   rfid: "490093C08F",
		   btname: " ",
		   btaddr: "8030DC0412D1",
		   doortype: "光交箱门",
		   doorcode: "A1"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| id       | int  |  锁具添加成功时返回的锁具id |

 
Example：
```json
       {
			"success":"",
			"s":1,
			“id”: 22
		}
```

### 删除锁具

- lockset/{lid}/del                      //lid为锁具id   
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |


Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |

 
Example：
```json
       {
			"success":"",
			"s":1
		}
```

### 获取最新APP版本号和路径

- appPackage/getversion            
- 支持的请求类型：POST

### post 
		  
- 请求格式：

| 参数 | 类型 | 描述                       | 默认值  | 是否必填 |
| ----   | ----    | ----------------           | ------     | --------      |
| k      |   String   |经过转换的编码格式，用于确定用户的权限  |   “”      |      YES    |


Example：

```json
		   {                        			           
		   k: "657F27A20859B5A1DC5D69EC83DC8D6F"
           }
```

- 响应格式: JSONObject
		  
| 参数名称 | 参数类型 | 描述 |
| -------- | -------- | ---- |
| success       | 无  |   表示成功返回   |
| s       | int  |  1表示成功，0为失败   |
| version | String |  app版本号   |
| path| String   | 存储app的路径|
| warn | String | 当没有上传痕迹时做出的提醒|
 
Example：
```json
       {
			"success":"",
			"s":1，
			"version": "1.0",
			"path":"static/public/appPackage/app.apk"
		}
```

### 修改时间和更新的内容

| 修改时间 |更新内容|
| -------- | ---- |
|2018年08月29日 10时31分59秒| 添加新增锁具接口：object/{oid}/lockset/new |
|  |  添加修改(更新）锁具接口：lockset/{lid}/update   |
|  |  添加删除锁具接口lockset/{lid}/del        | 
|2018年08月29日 13时50分02秒|修改 section/{sid}/object/downward1接口|
|  |在返回的lockset数组中给每组数据排好序，按A1,A2,B1,B2的顺序排好|
|  |在返回的lockset数组中添加该锁具的状态，lock_status,gate_status|
| 2018年08月29日 19时50分55秒 |将之前的section/{sid}/object/downward1接口，换成接口section/{sid}/object/downward|
|         |远程开锁修改上传的值，将index改成doorcode，即为门编号|
|  2018年08月30日 20时55分17秒 |   新增/getverson接口获取app版本相关内容|
|   |  修改section/{sid}/object/downward中的section描述，app专用|
|2018年08月31日 11时55分15秒|将接口/getverson 改成/getversion |
|    |将接口/getversion中version的类型改成String|
|                 |将接口中section/{username}/getsec没账号请求时username输入必须为0|
|2018年08月31日 18时04分09秒|修改接口section/{username}/getsec，将返回的代维单位做成树形结构插入区域中返回|
|2018年09月03日 11时27分56秒|修改一些重复，漏下的字段|
|2018年09月03日 15时57分57秒|/getversion修改为appPackage/getversion|
|2018年09月03日 22时22分58秒|修改字段|	
|2018年09月07日 18时00分00秒|修改 object/{oid}/update2接口|
| 接口新增锁id

